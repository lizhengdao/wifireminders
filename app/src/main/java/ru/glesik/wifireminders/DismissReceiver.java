/*
 * This file is part of Wi-Fi Reminders.
 *
 * Wi-Fi Reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wi-Fi Reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wi-Fi Reminders.  If not, see <http://www.gnu.org/licenses/>.
 */

package ru.glesik.wifireminders;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class DismissReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                "rules", Context.MODE_PRIVATE);
        // Get SSID, cell ID and notification tag from extras set in ConnectJobSchedulerService.
        String SSID = intent.getStringExtra("SSID");
        String cell = intent.getStringExtra("cell");
        int nId = intent.getIntExtra("nId", -1);
        int rulesCount = sharedPreferences.getInt("RulesCount", 0);

        // Disable all rules with given SSID or cell.
        for (int k = 1; k <= rulesCount; k++) {
            String currentSSID = sharedPreferences.getString("SSID" + k, "");
            String currentCell = sharedPreferences.getString("Cell" + k, "");
            if (currentSSID.equals(SSID) && currentCell.equals(cell)) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("Enabled" + k, false);
                editor.apply();
            }
        }

        // Update reminders list in main activity.
        Intent refreshIntent = new Intent();
        refreshIntent.setAction(RemindersListActivity.REFRESH_LIST);
        context.sendBroadcast(refreshIntent);

        // Hide notification.
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.cancel(nId);
    }

}
