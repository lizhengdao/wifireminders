package ru.glesik.wifireminders;

public class RemindersListDataModel {
    String name;
    Boolean enabled;

    public RemindersListDataModel(String name, Boolean enabled) {
        this.name = name;
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled() { enabled = true; }

    public void toggleEnabled() { enabled = !enabled; }
}
