package ru.glesik.wifireminders;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class RemindersListAdapter extends ArrayAdapter<RemindersListDataModel> implements View.OnClickListener {
    private ArrayList<RemindersListDataModel> dataSet;
    Context mContext;

    // View lookup cache.
    private static class ViewHolder {
        TextView reminderTextView;
        Switch reminderSwitch;
    }

    public RemindersListAdapter(ArrayList<RemindersListDataModel> data, Context context) {
        super(context, R.layout.reminders_list_row, data);
        this.dataSet = data;
        this.mContext = context;
    }

    @Override
    public void onClick(View v) {
        View parentRow = (View) v.getParent();
        ListView listView = (ListView) parentRow.getParent();
        final int position = listView.getPositionForView(parentRow);
        Object object = getItem(position);
        final RemindersListDataModel dataModel = (RemindersListDataModel) object;

        switch (v.getId())
        {
            case R.id.switchReminder:
                dataModel.toggleEnabled();
                SharedPreferences sharedPrefRulesSwitch = mContext.getSharedPreferences(
                        "rules", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPrefRulesSwitch.edit();
                editor.putBoolean("Enabled" + (position + 1),
                        dataModel.getEnabled());
                editor.commit();
                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position.
        RemindersListDataModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view.
        ViewHolder viewHolder;

        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.reminders_list_row, parent, false);
            viewHolder.reminderTextView = (TextView) convertView.findViewById(R.id.textReminder);
            viewHolder.reminderSwitch = (Switch) convertView.findViewById(R.id.switchReminder);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.reminderTextView.setText(dataModel.getName());
        viewHolder.reminderSwitch.setChecked(dataModel.getEnabled());
        viewHolder.reminderSwitch.setOnClickListener(this);
        // Return the completed view to render on screen.
        return convertView;
    }
}
