/*
 * This file is part of Wi-Fi Reminders.
 *
 * Wi-Fi Reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wi-Fi Reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wi-Fi Reminders.  If not, see <http://www.gnu.org/licenses/>.
 */

package ru.glesik.wifireminders;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.HashSet;
import java.util.List;

public class ConnectJobSchedulerService extends JobService {

    private static final String CHANNEL_ID = "wfrn";
    JobParameters mParams;

    @Override
    public boolean onStartJob(JobParameters params) {
        mParams = params;
        // Get current SSID.
        String currentSSID = getWifiName(this);
        // Get current CellIDs.
        HashSet<String> currentCells = new HashSet<>();
        final TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        assert telephony != null;
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                List<CellInfo> cellInfoList = telephony.getAllCellInfo();
                if (cellInfoList != null) {
                    for (CellInfo ci : cellInfoList) {
                        String cellId = "";
                        if (ci instanceof CellInfoGsm) {
                            CellIdentityGsm identityGsm = ((CellInfoGsm) ci).getCellIdentity();
                            cellId = Integer.toString(identityGsm.getCid());
                        } else if (ci instanceof CellInfoCdma) {
                            CellIdentityCdma identityCdma = ((CellInfoCdma) ci).getCellIdentity();
                            cellId = Integer.toString(identityCdma.getBasestationId());
                        } else if (ci instanceof CellInfoWcdma) {
                            CellIdentityWcdma identityWcdma = ((CellInfoWcdma) ci).getCellIdentity();
                            cellId = Integer.toString(identityWcdma.getCid());
                        } else if (ci instanceof CellInfoLte) {
                            CellIdentityLte identityLte = ((CellInfoLte) ci).getCellIdentity();
                            cellId = Integer.toString(identityLte.getCi());
                        }
                        currentCells.add(cellId);
                    }
                }
            }
        }
        Log.d("onStartJob", "Current SSID = " + currentSSID + ", current cells = " + currentCells.toString());
        if (currentCells.isEmpty()) {
            checkNetworks(currentSSID, "");
        } else {
            for (String cell : currentCells) {
                checkNetworks(currentSSID, cell);
            }
        }
        // Check if there are still enabled reminders.
        boolean activeExist = false;
        SharedPreferences sharedPreferences = getSharedPreferences(
                "rules", Context.MODE_PRIVATE);
        int rulesCount = sharedPreferences.getInt("RulesCount", 0);
        for (int i = 1; i <= rulesCount; i++) {
            if (sharedPreferences.getBoolean("Enabled"
                    + i, false)) {
                activeExist = true;
            }
        }
        if (!activeExist) {
            JobScheduler mJobScheduler;
            mJobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
            if (mJobScheduler != null) {
                Log.d("onStartJob", "No active rules, stopping polling job.");
                mJobScheduler.cancel(RemindersListActivity.WIFI_POLLING_JOB);
            }
        }
        Log.d("onStartJob", "Job finished.");
        jobFinished(mParams, false);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d("onStopJob", "Job stopped.");
        return false;
    }

    public String getWifiName(Context context) {
        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        assert manager != null;
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    String ssid = wifiInfo.getSSID();
                    // Remove quotes.
                    if (ssid.startsWith("\"") && ssid.endsWith("\"")) {
                        ssid = ssid.substring(1, ssid.length() - 1);
                    }
                    return ssid;
                }
            }
        }
        return null;
    }

    public void checkNetworks(String SSID, String cell) {
        SharedPreferences sharedPreferences = ConnectJobSchedulerService.this.getSharedPreferences(
                "rules", Context.MODE_PRIVATE);
        int rulesCount = sharedPreferences.getInt("RulesCount", 0);
        for (int k = 1; k <= rulesCount; k++) {
            boolean currentEnabled = sharedPreferences.getBoolean("Enabled"
                    + k, false);
            if (currentEnabled) {
                String currentSSID = sharedPreferences.getString("SSID"
                        + k, "");
                String currentCell = sharedPreferences.getString("Cell"
                        + k, "");
                Log.d("checkNetworks", "Comparing " + currentSSID + "/" + currentCell + " (from rules) and " + SSID + "/" + cell + " (current)");
                boolean match = false;
                if ((currentSSID.equals(SSID) && currentCell.equals(cell)) ||
                        (currentSSID.equals("") && currentCell.equals(cell)) ||
                        (currentSSID.equals(SSID)) && currentCell.equals("")) {
                    match = true;
                }
                if (match) {
                    String currentTitle = sharedPreferences.getString(
                            "Title" + k, "error");
                    String currentText = sharedPreferences.getString("Text"
                            + k, "error");
                    // Show reminder alert, vibrate and play sound.
                    Log.d("checkNetworks", "Showing reminder for " + currentSSID + "/" + currentCell + ": " + currentText);
                    showReminder(currentTitle, currentSSID, currentCell, currentText);
                }
            }
        }
    }

    public void showReminder(String title, String SSID, String cell, String text) {
        if (cell.equals("")) {
            title = title + " (" + SSID + ")";
        } else {
            if (SSID.equals("")) {
                title = title + " (" + cell + ")";
            } else {
                title = title + " (" + SSID + " & " + cell + ")";
            }
        }
        SharedPreferences sharedPrefSettings = PreferenceManager.getDefaultSharedPreferences(this);
        boolean vibrate = sharedPrefSettings.getBoolean("prefVibrate", true);
        String sound = sharedPrefSettings.getString("prefRingtone", "default");
        Uri soundURI = Uri.parse(sound);
        int icon = R.drawable.notify;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        CharSequence name = getString(R.string.channel_name);
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
        channel.setDescription(description);
        channel.enableLights(true);
        AudioAttributes aa = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build();
        channel.setSound(soundURI, aa);
        //channel.setLightColor(Color.GREEN);
        if (vibrate) {
            channel.enableVibration(true);
        }
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(channel);

        // Set unique id so that the notification is not duplicated if not dismissed.
        int nId = title.hashCode();

        Intent dismissIntent = new Intent(this, DismissReceiver.class);
        dismissIntent.putExtra("SSID", SSID);
        dismissIntent.putExtra("cell", cell);
        dismissIntent.putExtra("nId", nId);
        PendingIntent dismissPendingIntent = PendingIntent.getBroadcast(this, 0, dismissIntent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                ConnectJobSchedulerService.this, CHANNEL_ID)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(text))
                .setShowWhen(true)
                .setColor(ContextCompat.getColor(getApplicationContext(), R.color.wfr_green))
                .setOnlyAlertOnce(true)  // Don't interrupt if updated.
                .addAction(android.R.drawable.ic_menu_close_clear_cancel, getString(R.string.action_dismiss), dismissPendingIntent);

        Intent ni = new Intent(this, RemindersListActivity.class);
        ni.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pi = PendingIntent.getActivity(this, 0, ni, 0);
        mBuilder.setContentIntent(pi);
        notificationManager.notify(nId, mBuilder.build());
    }
}
